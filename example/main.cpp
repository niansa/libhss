#include <iostream>
#include <string>
#include <unistd.h>
#include <QIPC/ipc.hpp>
#include <hss.hpp>


int main() {
    QBiIPC ipc;
    ipc.create();

    HSS::run(ipc, "./libHSSTestChild.so");

    std::cout << ipc.recv() << std::endl
              << ipc.recv_raw<size_t>() << std::endl;
    ipc.send("/etc/passwd");
    for (std::string line = ""; line != "End."; line = ipc.recv()) {
        if (!line.empty()) {
            std::cout << line << std::endl;
        }
    }
}
