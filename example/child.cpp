#include <string>
#include <fstream>
#include <sys/mman.h>
#include <QIPC/ipc.hpp>


extern "C"
void entry(QBiIPC& ipc) {
    ipc.send("Hello world!");
    ipc.send_raw(size_t(1234567890));

    auto fname = ipc.recv();
    ipc.send("Trying to open file: "+fname);
    std::ifstream f(fname);
    if (f.fail()) {
        ipc.send(std::string("Failed: ")+strerror(errno));
    } else {
        ipc.send("Success! First line:");
        std::string line;
        std::getline(f, line);
        ipc.send(line);
        f.close();
    }

    ipc.send("End.");
}
