#ifndef _HSS_HPP
#define _HSS_HPP
#include <iostream>
#include <string>
#include <unistd.h>
#include <QIPC/ipc.hpp>



namespace HSS {
inline pid_t run(QBiIPC& ipc, const std::string& file) {
    auto pid = fork();
    if (pid == 0) {
        execlp("HSSChildLauncher", "child",
               std::to_string(ipc.out.get_in()).c_str(),
               std::to_string(ipc.out.get_out()).c_str(),
               std::to_string(ipc.in.get_in()).c_str(),
               std::to_string(ipc.in.get_out()).c_str(),
               file.c_str(),
               nullptr);
        std::cerr << "Failed to run HSSChildLauncher" << std::endl;
        abort();
    }
    return pid;
}
}
#endif
